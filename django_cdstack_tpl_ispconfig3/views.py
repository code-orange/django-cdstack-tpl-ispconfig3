from django.template import engines

import copy
import os

from django_cdstack_deploy.django_cdstack_deploy.func import generate_config_static
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import handle as handle_deb
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import get_os_release
from django_cdstack_tpl_deb_stretch.django_cdstack_tpl_deb_stretch.views import (
    get_apt_packages,
    get_apt_sources,
    get_apt_prefs,
)
from django_cdstack_tpl_mariadb.django_cdstack_tpl_mariadb.views import (
    handle as handle_mariadb,
)
from django_cdstack_tpl_phpworker.django_cdstack_tpl_phpworker.views import (
    handle as handle_phpworker,
)
from django_cdstack_tpl_whclient.django_cdstack_tpl_whclient.views import (
    handle as handle_whclient,
)
from django_cdstack_tpl_modsecurity_main.django_cdstack_tpl_modsecurity_main.views import (
    handle as handle_modsecurity_main,
)


def handle(zipfile_handler, template_opts, cmdb_host, skip_handle_os=False):
    django_engine = engines["django"]

    module_prefix = "django_cdstack_tpl_ispconfig3/django_cdstack_tpl_ispconfig3"

    if "apt_packages" not in template_opts:
        template_opts["apt_packages"] = get_apt_packages(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "fail2ban_apps" not in template_opts:
        template_opts["fail2ban_apps"] = copy.deepcopy(template_opts["apt_packages"])

    if "apt_sources" not in template_opts:
        template_opts["apt_sources"] = get_apt_sources(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "apt_preferences" not in template_opts:
        template_opts["apt_preferences"] = get_apt_prefs(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "os_release" not in template_opts:
        template_opts["os_release"] = get_os_release(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    generate_config_static(zipfile_handler, template_opts, module_prefix)
    handle_modsecurity_main(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_whclient(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_mariadb(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_phpworker(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    return True
